package diagramadeflujo;

import Clases.Docu;
import Clases.Entrasal;
import Clases.Infin;
import Clases.Proceso;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;

/**
 *
 * @author eduardovalenzuela
 */
public class DiagramaController implements Initializable {
    
    @FXML private Pane controles;
    @FXML private Pane mostrar;
    @FXML private ScrollPane scroll;
    @FXML private Button linea;
    @FXML private Button entradaSalida;
    @FXML private Button inicioFin;
    @FXML private Button etapa;
    @FXML private Button documento;
    @FXML private Button eliminar;
    @FXML private Button eliminarElemento;
    @FXML private Button ejecutar;
    
    
    Group dibujo= new Group();
    double orgSceneX, orgSceneY;
    double orgTranslateX, orgTranslateY, ejeX, ejeY, ejeXauxiliar, ejeYauxiliar, ejeXauxiliar2, ejeYauxiliar2;
    boolean seguir = true, dibujado=true, inicio=true, finalCil=true, seguir2=true;
    String respuesta = "Inicio";
    ArrayList<Group> diagrama = new ArrayList<>();
    ArrayList<Group> lineas = new ArrayList<>();
    int num=0;
    
    
    
    @FXML
    private void linea(ActionEvent event) {
        Label labelAuxiliar = (Label) diagrama.get(diagrama.size()-1).getChildren().get(diagrama.get(diagrama.size()-1).getChildren().size()-2);
        if (diagrama.size()>=2) {
            JuntarDiagrama();
            
            if ("Final".equals(labelAuxiliar.getText())) {
                linea.setDisable(true);
                entradaSalida.setDisable(true);
                inicioFin.setDisable(true);
                etapa.setDisable(true);
                documento.setDisable(true);
                
                
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("INFORMACION");
                alert.setHeaderText("Informacion para el usuario");
                alert.setContentText("El Diagrama ya ha terminado.");

                alert.showAndWait();
                
            }
        }
        else{
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText("Ooops, existe un error.");
            alert.setContentText("Deben existir como minimo, dos figuras.");

            alert.showAndWait();
        }
        
        
        
        
 
    }
    
    @FXML
    private void Rectangulo(ActionEvent event) {
        seguir = true; dibujado=true;
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Cuadro de Dialogo");
        dialog.setHeaderText("Escribir...");
        dialog.setContentText("Ingrese un proceso: ");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            String nombre = result.get();
            mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouse) {
                    if (seguir) {
                        ejeX=mouse.getX();
                        ejeY=mouse.getY();
                        seguir = false;
                    }
                    verificarOrillas();
                    boolean dibujar2 = verificarFigura();
                    if (dibujado && dibujar2) {
                        Proceso proce = new Proceso();
                        Group nuevo = proce.Dibujar(ejeX, ejeY, nombre);
                        mostrar.getChildren().add(nuevo);
                        diagrama.add(nuevo);
                        dibujado=false;

                    }
                }
            });
        }
        
        
 
    }
    
    @FXML
    private void Trapecio(ActionEvent event) {
        seguir = true; dibujado=true;
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Cuadro de Dialogo");
        dialog.setHeaderText("Escribir...");
        dialog.setContentText("Ingrese un proceso: ");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            String nombre = result.get();
            mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouse) {
                    if (seguir) {
                        ejeX=mouse.getX();
                        ejeY=mouse.getY();
                        seguir = false;
                    }
                    boolean dibujado2 = verificarFigura();
                    verificarOrillas();
                    if (dibujado && dibujado2) {
                        Entrasal ensal = new Entrasal();
                        Group nuevo = ensal.Dibujar(ejeX, ejeY, nombre);
                        mostrar.getChildren().add(nuevo);
                        diagrama.add(nuevo);
                        dibujado=false;
                    }
                }
            });
        }
 
    }
    
    @FXML
    private void Cilindro(ActionEvent event) {
        seguir = true; dibujado=true;
        bloquearAlInicio(false);
        if (!inicio && !finalCil) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText("ERROR importante");
            alert.setContentText("El algoritmo ya se ha terminado :(\nNo se puede crear mas inicio/fin :)\nVuelva pronto...");

            alert.showAndWait();
        }
        else{
            if (inicio) {
                respuesta = "Inicio";
                inicio = false;
            }
            else if (finalCil) {
                respuesta = "Final";
                finalCil = false;
            }

            mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouse) {
                    if (seguir) {
                        ejeX=mouse.getX();
                        ejeY=mouse.getY();
                        seguir = false;
                    }
                    verificarOrillas();
                    boolean dibujar2 = verificarFigura();
                    if (dibujado && dibujar2) {
                        Infin infin = new Infin();
                        Group nuevo = infin.Dibujar(ejeX, ejeY, respuesta);
                        mostrar.getChildren().addAll(nuevo);
                        diagrama.add(nuevo);
                        dibujado=false;
                    }
                }
            });
        }
        
        
        
        
 
    }
    @FXML
    private void Documento(ActionEvent event) {
        seguir = true; dibujado=true;
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Cuadro de Dialogo");
        dialog.setHeaderText("Escribir...");
        dialog.setContentText("Ingrese un proceso: ");
        Optional<String> result = dialog.showAndWait();
        
        if (result.isPresent()){
            String nombre = result.get();
            mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouse) {
                    if (seguir) {
                        ejeX=mouse.getX();
                        ejeY=mouse.getY();
                        seguir = false;
                    }
                    verificarOrillas();
                    boolean dibujado2 = verificarFigura();
                    if (dibujado && dibujado2) {
                        Docu docu = new Docu();
                        Group nuevo = docu.Dibujar(ejeX, ejeY, nombre);
                        mostrar.getChildren().add(nuevo);
                        diagrama.add(nuevo);
                        dibujado=false;
                    }
                }
            });
        }
 
    }
    @FXML
    private void Eliminar(ActionEvent event) {
        mostrar.getChildren().clear();
        diagrama.clear();
        lineas.clear();
        inicio=true;
        finalCil=true;
        inicioFin.setDisable(false);
        linea.setDisable(false);
        entradaSalida.setDisable(false);
        etapa.setDisable(false);
        documento.setDisable(false);
    }
    
    @FXML
    private void EliminarElemento(ActionEvent event) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("INFORMACION");
        alert.setHeaderText("Informacion para el usuario");
        alert.setContentText("En mantencion... :)");

        alert.showAndWait();
    }
    @FXML
    private void Ejecutar(ActionEvent event) { 
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("INFORMACION");
        alert.setHeaderText("Informacion para el usuario");
        alert.setContentText("En mantencion... :)");

        alert.showAndWait();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ImageView img1 = new ImageView("Imagenes/Linea.png");
        ImageView img2 = new ImageView("Imagenes/Inicio-Fin.png");
        ImageView img3 = new ImageView("Imagenes/Etapa.png");
        ImageView img4 = new ImageView("Imagenes/Entrada-Salida.png");
        ImageView img5 = new ImageView("Imagenes/Documento.png");
        ImageView img6 = new ImageView("Imagenes/Eliminar.png");

        img1.setFitWidth(85);
        img1.setFitHeight(75);
        img2.setFitWidth(85);
        img3.setFitWidth(85);
        img4.setFitWidth(85);
        img5.setFitWidth(85);
        img5.setFitHeight(75);
        img6.setFitWidth(85);
        img6.setFitHeight(75);
        

        linea.setGraphic(img1);
        entradaSalida.setGraphic(img4);
        inicioFin.setGraphic(img2);
        etapa.setGraphic(img3);
        documento.setGraphic(img5);
        eliminar.setGraphic(img6);
        bloquearAlInicio(true);
        
    }
    
    EventHandler<MouseEvent> GroupOnMousePressedEventHandler = 
        new EventHandler<MouseEvent>() {
 
        @Override
        public void handle(MouseEvent t) {
            dibujado=false;
            orgSceneX = t.getSceneX();
            orgSceneY = t.getSceneY();
            orgTranslateX = ((Group)(t.getSource())).getTranslateX();
            orgTranslateY = ((Group)(t.getSource())).getTranslateY();
            
        }
    };
    
    
     
    EventHandler<MouseEvent> GroupOnMouseDraggedEventHandler = 
        new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent t) {
            dibujado=false;
            double offsetX = t.getSceneX() - orgSceneX;
            double offsetY = t.getSceneY() - orgSceneY;
            double newTranslateX = orgTranslateX + offsetX;
            double newTranslateY = orgTranslateY + offsetY;

            ((Group)(t.getSource())).setTranslateX(newTranslateX);
            ((Group)(t.getSource())).setTranslateY(newTranslateY);
        }
    };
    
    public boolean verificarFigura(){
        if (diagrama.size()==1) {
            Group auxGroup = diagrama.get(0);
            Line aux = (Line) auxGroup.getChildren().get(0);
            Line aux2 = (Line) auxGroup.getChildren().get(diagrama.get(0).getChildren().size()-1);
            if (ejeY>=aux.getStartY() && ejeY<=aux2.getStartY()+30) {
                return false;
            }
        }
        else{
            for (int i = 0; i < diagrama.size(); i++) {
                Group auxGroup = diagrama.get(i);
                Line aux = (Line) auxGroup.getChildren().get(0);
                Line aux2 = (Line) auxGroup.getChildren().get(diagrama.get(i).getChildren().size()-1);
                if (ejeX>=aux.getStartX() && ejeX<=aux.getEndX() && ejeY>=aux.getStartY() && ejeY<=aux2.getStartY()) {
                    return false;
                }
            }
            for (int i = 0; i < diagrama.size()-1; i++) {
                Group auxGroup = diagrama.get(i);
                Group auxGroup2 = diagrama.get(i+1);
                Line aux = (Line) auxGroup.getChildren().get(0);
                Line aux2 = (Line) auxGroup2.getChildren().get(0);
                if (ejeY>=aux.getStartY() && ejeY<=aux2.getStartY()) {
                    return false;
                }
            }
            for (int i = 0; i < lineas.size(); i++) {
                Line auxLine = (Line) lineas.get(i).getChildren().get(1);
                if (ejeY>=auxLine.getStartY()-50 && ejeY<=auxLine.getEndY()+50) {
                    return false;
                }
            }
        }
        return true;
        
    }
    
    public void JuntarDiagrama(){
        mostrar.getChildren().clear();
        dibujo.getChildren().clear();
        lineas.clear();
        
        for (int i = 0; i < diagrama.size()-1; i++) {
            Line aux = (Line) diagrama.get(i).getChildren().get(diagrama.get(i).getChildren().size()-1);
            Line aux2 = (Line) diagrama.get(i+1).getChildren().get(0);
            double promedio1X = (aux.getEndX()+aux.getStartX())/2;
            double promedio2X = (aux2.getEndX()+aux2.getStartX())/2;
            Line finall = new Line(promedio1X, aux.getStartY(), promedio2X, aux2.getEndY()-10);
            Line finall2 = new Line(promedio2X-10, aux2.getStartY()-10, promedio2X+10, aux2.getEndY()-10);
            Line finall3 = new Line(promedio2X-10, aux2.getStartY()-10, promedio2X, aux2.getEndY());
            Line finall4 = new Line(promedio2X+10, aux2.getStartY()-10, promedio2X, aux2.getEndY());
            Group nuevoaux = new Group();
            nuevoaux.getChildren().addAll(finall, finall2, finall3, finall4);
            lineas.add(nuevoaux);
        }
        dibujo.getChildren().addAll(diagrama);
        dibujo.getChildren().addAll(lineas);
        mostrar.getChildren().add(dibujo);
    }
    
    public void verificarOrillas(){
        if ( ejeX>=0.0 && ejeX<=10.0 || ejeY>=0.0 && ejeY<=10.0) {
            if (diagrama.isEmpty()) {
                ejeX = mostrar.getPrefWidth()/2;
                ejeY = 50;
            }
            else{
                Group auxGroup = diagrama.get(diagrama.size()-1);
                Line auxLine = (Line) auxGroup.getChildren().get(auxGroup.getChildren().size()-1);
                double promedioX = (auxLine.getStartX()+auxLine.getEndX())/2;
                double promedioY = (auxLine.getStartY()+auxLine.getEndY())/2;
                ejeX=promedioX;
                ejeY = promedioY+50; 
            }
        }
        if (ejeX>=mostrar.getPrefWidth()-50 && ejeX<=mostrar.getPrefWidth() && verificarFigura()) {
            if (diagrama.isEmpty()) {
                ejeX = mostrar.getPrefWidth()/2;
                ejeY = 50;
            }
            else{
                mostrar.setPrefWidth(ejeX+100);
                scroll.setContent(mostrar);
            }
        }
        if (ejeY>=mostrar.getPrefHeight()-50 && ejeY<=mostrar.getPrefHeight() && verificarFigura()) {
            if (diagrama.isEmpty()) {
                ejeX = mostrar.getPrefWidth()/2;
                ejeY = 50;
            }
            else{
                mostrar.setPrefHeight(ejeY+100);
                scroll.setContent(mostrar);
            }
        }
    }
    
    public void bloquearAlInicio(boolean inicio) {
        if (inicio) {
            linea.setDisable(true);
            entradaSalida.setDisable(true);
            etapa.setDisable(true);
            documento.setDisable(true);
        }
        else {
            linea.setDisable(false);
            entradaSalida.setDisable(false);
            etapa.setDisable(false);
            documento.setDisable(false);            
        }
    }
    
}
