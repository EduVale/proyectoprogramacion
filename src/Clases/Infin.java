/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.CubicCurve;
import javafx.scene.shape.Line;

/**
 *
 * @author Nicolas
 */
public class Infin extends Figuras{

    @Override
    public Group Dibujar(double x1, double y1, String nom) {
        Group nuevo = new Group();
        /*LINEA SUPERIOR*/
        Line nueva = new Line(x1-50, y1, x1+50, y1);
        nueva.setStroke(Color.CORAL);
        /*CURVAS DE LOS COSTADOS*/
        CubicCurve cubic = new CubicCurve();
        cubic.setStartX(x1+50);
        cubic.setStartY(y1);
        cubic.setControlX1(x1+75);
        cubic.setControlY1(y1);
        cubic.setControlX2(x1+75);
        cubic.setControlY2(y1+50);
        cubic.setEndX(x1+50);
        cubic.setEndY(y1+50);
        /*COLOR Y ANCHO DE LAS CURVAS*/
        cubic.setFill(Color.CORAL);
        cubic.setStrokeWidth(2);
        cubic.setStroke(Color.CORAL);
        /*LINEA INFERIOR*/
        Line nueva2 = new Line(x1-50, y1+50, x1+50, y1+50);
        nueva2.setStroke(Color.CORAL);
        /*CURVAS DE LOS COSTADOS*/
        CubicCurve cubic2 = new CubicCurve();
        cubic2.setStartX(x1-50);
        cubic2.setStartY(y1);
        cubic2.setControlX1(x1-75);
        cubic2.setControlY1(y1);
        cubic2.setControlX2(x1-75);
        cubic2.setControlY2(y1+50);
        cubic2.setEndX(x1-50);
        cubic2.setEndY(y1+50);       
        /*COLOR Y ANCHO DE LAS CURVAS*/
        cubic2.setFill(Color.CORAL);
        cubic2.setStrokeWidth(2);
        cubic2.setStroke(Color.CORAL);       
        /*TEXTO*/
        Label label = new Label(nom);
        label.setLayoutX((x1+x1)/2);
        label.setLayoutY((y1+y1+50)/2);
        label.setTextFill(Color.WHITE);
        label.setStyle("-fx-background-color: transparent;");
        Pane nuevoPane = rellenarColor(x1, y1, "coral", 0);
        nuevo.getChildren().addAll(nueva, cubic, cubic2, nuevoPane, label, nueva2);
        return nuevo;
    }

    @Override
    public Pane rellenarColor(double x1, double y1, String color, double i) {
        /*ANCHO Y ALTURA DEL RECTANGULO (PITAGORAS)*/
        double ancho = Math.sqrt(Math.pow((x1+i + 50) - (x1-i - 50), 2) + Math.pow(y1 - y1, 2));
        double alto = Math.sqrt(Math.pow((x1+i + 50) - (x1+i + 50), 2) + Math.pow((y1 + 50) - y1, 2));
        /*COLOR DE FONDO*/
        Pane fondo = new Pane();
        fondo.setStyle("-fx-background-color: " + color + ";");
        fondo.setPrefSize(ancho, alto);
        fondo.setLayoutX(x1-50-i);
        fondo.setLayoutY(y1);    
        
        return fondo;}
    
}
