/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;
import javafx.scene.Group;
import javafx.scene.shape.Line;

/**
 *
 * @author Nicolas
 */

public class Flujo {

    public Flujo() {
    }
    
    public Group Dibujar(double x1, double y1, double x2, double y2){
        Group nuevo = new Group();
        Line nueva = new Line(x1, y1, x2, y2);
        nuevo.getChildren().addAll(nueva);
        return nuevo;
           
    }
}
