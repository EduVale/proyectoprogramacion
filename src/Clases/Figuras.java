package Clases;

import javafx.scene.Group;
import javafx.scene.layout.Pane;

public abstract class Figuras{
    public abstract Group Dibujar(double x1, double y1, String nom);
    public abstract Pane rellenarColor(double x1, double y1, String color, double i); 
}
